/*
 * ApplicationLayer.h
 *
 * Created: 3/5/2015 1:30:37 AM
 *  Author: Brad
 */ 


#ifndef APPLICATIONLAYER_H_
#define APPLICATIONLAYER_H_

#ifdef __cplusplus
extern "C" {
	#endif

int r_main(void);
void test_services(void);
void run_subscribe();
void run_publish();
void test_error_LED();
void test_now();
void test_RR();
void test_SYSTEM();
void inf_loop();
void delay_and_terminate();
void create_SYSTEM_task();
void test_generate_good_schedule();
void test_generate_bad_schedule();
void finished();
void test_publish_all();
void run_publish_periodic();
void test_publish_from_interrupt();
void profile();
void run_perodic_5_then_terminate();
void run_perodic_10_then_terminate();

#ifdef __cplusplus
}
#endif

#endif /* APPLICATIONLAYER_H_ */