/**
 * @file   error_code.h
 *
 * @brief Error messages returned in OS_Abort().
 *        Green errors are initialization errors
 *        Red errors are runt time errors
 *
 * CSC 460/560 Real Time Operating Systems - Mantis Cheng
 *
 * @author Scott Craig
 * @author Justin Tanner
 */
#ifndef __ERROR_CODE_H__
#define __ERROR_CODE_H__

enum {

/** GREEN ERRORS -- Initialize time errors. */
    
/** PPP invalid -- Names must be in range [0 .. MAXPROCESS] */
ERR_1_PPP_NAME_OUT_OF_RANGE,

/** PERIODIC start, period, or wcet was not specified */
ERR_2_CREATE_PERIODIC_INVALID_ARGS,

/** PERIODIC task assigned name IDLE */
ERR_3_PERIODIC_NAMED_IDLE,

/** Time interval 0 in PPP array */
ERR_4_PERIODIC_SCHEDULING_CONFLICT,


/** RED ERRORS -- Run time errors. */

/** User called OS_Abort() */
ERR_RUN_5_USER_CALLED_OS_ABORT,

/** Too many tasks created. Only allowed MAXPROCESS at any time.*/
ERR_RUN_6_TOO_MANY_TASKS,

/** PERIODIC task still running at end of time slot. */
ERR_RUN_7_PERIODIC_TOOK_TOO_LONG,

/** ISR made a request that only tasks are allowed. */
ERR_RUN_8_ILLEGAL_ISR_KERNEL_REQUEST,

/** RTOS Internal error in handling request. */
ERR_RUN_9_RTOS_INTERNAL_ERROR,

/** Service subscribe() on uninitialized service */
ERR_RUN_10_SUBSCRIBE_TO_BAD_SERVICE,

/** PERIODIC task shouldn't ever call wait */
ERR_RUN_11_PERIODIC_CALLED_WAIT,

/* Should never happen */
ERR_RUN_12_RTOS_INTERNAL_ERROR,

/** Service publish() on uninitialized service */
ERR_RUN_13_PUBLISH_ON_BAD_SERVICE,

/** Error for if more services than MAXSERVICES being created */
ERR_RUN_14_TOO_MANY_SERVICES

};


#endif
