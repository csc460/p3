/*
 * RoombaVer3.c
 *
 * Created: 2015-03-05 13:32:56
 *  Author: Daniel
 */ 

#define F_CPU 16000000UL
/** The RTOS timer's prescaler divisor */
#define TIMER_PRESCALER 8

/** The number of clock cycles in one "tick" or 5 ms */
#define TICK_CYCLES     (((F_CPU / TIMER_PRESCALER) / 1000) * TICK)

extern "C" void TIMER3_COMPA_vect(void) __attribute__ ((signal));

#include <avr/io.h>
#include "os.h"
#include "roomba/roomba.h"
#include "roomba/roomba_sci.h"
#include "timer.h"
#include "ir/ir.h"
#include "cops_and_robbers.h"
#include "uart/uart.h"
#include "radio/radio.h"
#include "RoombaVer3.h"
#include "usart.h"

SERVICE* radio_receive_service;
SERVICE* ir_receive_service;
OS_TIMER roomba_timeout_timer;
volatile uint8_t is_roomba_timedout = 0;

ROOMBAS roomba_num = ROOMBA2;
volatile ROOMBA_STATUSES current_roomba_status = ROOMBA_NOT_IT;


//Used to stop the Roomba when it gets shot.
void roomba_Drive( int16_t velocity, int16_t radius )
{
	Roomba_Send_Byte(DRIVE);
	Roomba_Send_Byte(HIGH_BYTE(velocity));
	Roomba_Send_Byte(LOW_BYTE(velocity));
	Roomba_Send_Byte(HIGH_BYTE(radius));
	Roomba_Send_Byte(LOW_BYTE(radius));
}

void radio_rxhandler(uint8_t pipenumber) {
	Service_Publish(radio_receive_service, 0);
}

//Handle expected IR values, record unexpected values to pass on via radio. 
//	(Get Roomba state via state packets)
void ir_rxhandler() {
	USART_Send_string("IR Received\n\t");
	uint8_t ir_value = IR_getLast();
	USART_Send_int(ir_value);
	USART_Send_string("\n");
	
	Service_Publish(radio_receive_service, 0);
	if(ir_value == IR_SHOOT)
	{
		current_roomba_status = ROOMBA_SHOT;
		USART_Send_string("Transmitting Shot Packet To Basestation\n");
		//Handle the packets
		RADIO_RX_STATUS result;
		radiopacket_t packet;

		// 1. Handshake with basestation
		Radio_Set_Tx_Addr(station_addr);

		// send the sensor packet back to the remote station.
		packet.type = ROOMBA_STATUS_UPDATE;
		packet.payload.status_info.roomba_number = roomba_num;
		Radio_Transmit(&packet, RADIO_RETURN_ON_TX);
		current_roomba_status = ROOMBA_NOT_IT;
	}
}

void spinAndBlink()
{
	PORTB = DDRB;
	//Spin in a circle for about 2s
	roomba_Drive(500, -1);
	//Blink 5 times
	for(int i = 0; i < 4; i++)
	{
		_delay_ms(500);
	}
	roomba_Drive(0,0);
	current_roomba_status = ROOMBA_IT;
	PORTB = 0;
}

void handleRoombaPacket(radiopacket_t *packet) {
	USART_Send_string("Roomba Packet Received\n");
	PORTG = DDRG;
	PORTG = 0;
	
	//Filter out unwanted commands.
	if (packet->payload.command.command == START ||
		packet->payload.command.command == BAUD ||
		packet->payload.command.command == SAFE ||
		packet->payload.command.command == FULL ||
		packet->payload.command.command == SENSORS)
	{
		return;
	}
	
	//If the Roomba is in the tagged state, it should not start moving.
	//Pass the command to the Roomba.
	Roomba_Send_Byte(packet->payload.command.command);
	for (int i = 0; i < packet->payload.command.num_arg_bytes; i++)
	{
		Roomba_Send_Byte(packet->payload.command.arguments[i]);
	}
	
	/* DON'T ACK EVERY PACKET
	// Set the radio's destination address to be the remote station's address
	Radio_Set_Tx_Addr(packet->payload.command.sender_address);
	
	// Update the Roomba sensors into the packet structure that will be transmitted.
	//Roomba_UpdateSensorPacket(1, &packet.payload.sensors.sensors);
	//Roomba_UpdateSensorPacket(2, &packet.payload.sensors.sensors);
	//Roomba_UpdateSensorPacket(3, &packet.payload.sensors.sensors);

	// send the sensor packet back to the remote station.
	packet->type = SENSOR_DATA;
	packet->payload.sensors.roomba_number = roomba_num;
	Radio_Transmit(packet, RADIO_RETURN_ON_TX);
	*/
	
	handleStatusPacket(packet);
}

void handleIRPacket(radiopacket_t *packet) {
	USART_Send_string("IR Packet Received\n");
	
	//Transmit data
	if(packet->payload.ir_command.ir_command == SEND_BYTE) {
		cli();
		USART_Send_string("\tIR Transmit\n");
		IR_transmit(packet->payload.ir_command.ir_data);
		sei();
	}
	
	if(packet->payload.ir_command.ir_command == AIM_SERVO) {
		//Aim the servo!
	}
	
	/*
	// Set the radio's destination address to be the remote station's address
	Radio_Set_Tx_Addr(packet->payload.command.sender_address);
	
	//Return last IR command recieved
	packet->type = IR_DATA;
	
	Radio_Transmit(packet, RADIO_RETURN_ON_TX);
	*/	
	handleStatusPacket(packet);
}
void handleStatusPacket(radiopacket_t *packet) {
	if (current_roomba_status == ROOMBA_NOT_IT && packet->payload.command.it == roomba_num)
	{
		current_roomba_status = ROOMBA_TAGGED;
		Task_Create_System(spinAndBlink, 0);
		PORTB = DDRB;
		// Spawn a periodic task to shoot
		current_roomba_status = ROOMBA_IT;
		Task_Create_Periodic(roombaShoot,0,200,195,200);
	}
	
	else if(current_roomba_status == ROOMBA_IT && packet->payload.command.it != roomba_num)
	{
		current_roomba_status = ROOMBA_NOT_IT;
		PORTB = 0;
	}
	
	/*
	Radio_Set_Tx_Addr(packet->payload.command.sender_address);
	packet->type = ROOMBA_STATUS_UPDATE;
	packet->payload.status_info.roomba_number = roomba_num;
	packet->payload.status_info.roomba_status = current_roomba_status;
	
	Radio_Transmit(packet, RADIO_RETURN_ON_TX);
	*/		
}

void rr_roomba_controler() {
	//Start the Roomba for the first time.
	int x;
	
	Roomba_Init();
	
	for(;;) {
		Service_Subscribe(radio_receive_service, &x);
		
		//Restart the timeout timer.
		timer_reset(&roomba_timeout_timer);
		timer_resume(&roomba_timeout_timer);
		
		if(is_roomba_timedout) {
			is_roomba_timedout = 0;
			Roomba_Init();
		}
		
		/*
		// Stop the Roomba if it is dead.
		if(current_roomba_status == ROOMBA_DEAD) {
			roomba_Drive(0,500);
		}
		*/
		
		//Handle the packets
		RADIO_RX_STATUS result;
		radiopacket_t packet;
		do {
			result = Radio_Receive(&packet);
			if(result == RADIO_RX_SUCCESS || result == RADIO_RX_MORE_PACKETS) {				
				//Handle Roomba Commands
				if(packet.type == COMMAND) {
					handleRoombaPacket(&packet);
				}
				
				//Handle IR Commands
				else if(packet.type == IR_COMMAND) {
					handleIRPacket(&packet);
				}
				
				
				/*
				// Handle Roomba Status Packets
				else if(packet.type == REQUEST_ROOMBA_STATUS_UPDATE)
				{
					handleStatusPacket(&packet);
				}
				*/
				
				/*
				if(current_roomba_status == ROOMBA_IT)
				{
					cli();
					USART_Send_string("Periodic IR Transmit\n");
					IR_transmit(IR_SHOOT);
					sei();
				}
				*/			
			}
		} while (result == RADIO_RX_MORE_PACKETS);
	}
}

void handleRadioPackets()
{
	for(;;)
	{
		for(int i = 0; i < 20; i++)
		{
			//Handle the packets
			RADIO_RX_STATUS result;
			radiopacket_t packet;
			do {
				result = Radio_Receive(&packet);
				if(result == RADIO_RX_SUCCESS || result == RADIO_RX_MORE_PACKETS) {				
					//Handle Roomba Commands
					if(packet.type == COMMAND) {
						handleRoombaPacket(&packet);
					}
				
					//Handle IR Commands
					else if(packet.type == IR_COMMAND) {
						handleIRPacket(&packet);
					}		
				}
			} while (result == RADIO_RX_MORE_PACKETS);
		}
		Task_Next();
	}
}


//Check if the Roomba has been idle long enough to
//	put it to sleep. This timer is reset every time a
//	packet arrives.
void per_roomba_timeout() {
	timer_reset(&roomba_timeout_timer);
	timer_resume(&roomba_timeout_timer);
	for(;;){
		if(timer_value(&roomba_timeout_timer) > 60000) {
			timer_pause(&roomba_timeout_timer);			
			if(!is_roomba_timedout) {
				is_roomba_timedout = 1;
				Roomba_Finish();
			}
		}
	}
}

void roombaShoot()
{
	for(;;)
	{
		cli();
		USART_Send_string("Periodic IR Transmit\n");
		IR_transmit(IR_SHOOT);
		sei();
		if (current_roomba_status == ROOMBA_IT)
		{
			Task_Next();
		}
		else
		{
			Task_Terminate();
		}
	}
}

void roombaShootRR()
{
	for(;;)
	{
		cli();
		USART_Send_string("Periodic IR Transmit\n");
		IR_transmit(IR_SHOOT);
		sei();
	}
}

int r_main(void)
{
	USART_Init(MYUBRR); // Initializes the serial communication
	// Pin 12: Port B. It LED
	DDRB = (uint8_t)(_BV(DDB6));
	PORTB = 0;
	
	// RR Controlling Roomba: Pin 41
	DDRG = (uint8_t)(_BV(DDG0));
	PORTG = 0;
	
	//Turn off radio power.
	DDRL |= (1 << PL2);
	PORTL &= ~(1<<PL2);
	_delay_ms(500);
	PORTL |= (1<<PL2);
	_delay_ms(500);
	
	//Initialize radio.
	cli();
	Radio_Init();
	IR_init();
	Radio_Configure_Rx(RADIO_PIPE_0, ROOMBA_ADDRESSES[roomba_num], ENABLE);
	Radio_Configure(RADIO_2MBPS, RADIO_HIGHEST_POWER);
	sei();
	
	radio_receive_service = Service_Init();
	ir_receive_service = Service_Init();
	Task_Create_RR(rr_roomba_controler,0);
 	
	// Test SHooter 
	// Task_Create_Periodic(roombaShoot,0,100,95,50);
	// current_roomba_status = ROOMBA_IT;
}