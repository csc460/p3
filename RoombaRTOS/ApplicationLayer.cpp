/*
 * ApplicationLayer.cpp
 *
 * Created: 2015-03-02 6:16:15 PM
 *  Author: Daniel
 */ 
#include "ApplicationLayer.h"
#include "os.h"
#include "usart.h"
#include <util/delay.h>		// Including the avr delay lib
#include <avr/io.h>
#include <avr/interrupt.h>
#define NUMTASKS 3

/* kernel.h clock setup */
#define F_CPU 16000000UL
/** The RTOS timer's prescaler divisor */
#define TIMER_PRESCALER 8

/** The number of clock cycles in one "tick" or 5 ms */
#define TICK_CYCLES     (((F_CPU / TIMER_PRESCALER) / 1000) * TICK)

extern "C" void TIMER3_COMPA_vect(void) __attribute__ ((signal));

SERVICE* service;

int start_interrupt = 0;

int r_main()
{
	// Set up the UART
	USART_Init(MYUBRR); // Initializes the serial communication
	
	// test_now();
	
	int i = 1;
	while(i <= 3 )
	{
		USART_Send_string("ApplicationLayer Init: ");
		USART_Send_int(i);
		USART_Send_string("\n");
		_delay_ms(50000);
		i++;
	}
	// test_services();
	test_error_LED();
	// test_RR();
	// test_SYSTEM();
	// test_generate_good_schedule();
	// test_generate_bad_schedule();
	// test_publish_all();
	// test_publish_from_interrupt();
	// profile();
}

// This function is passed as an argument to tasks made when testing subscribing
void run_subscribe()
{
	int16_t v;
	Service_Subscribe(service, &v);
	volatile int16_t* arg = Task_GetArg();
	USART_Send_string("Task #");
	USART_Send_int(*arg);
	USART_Send_string(" value: ");
	USART_Send_int(v);
	USART_Send_string("\n");
	// Terminate is important when running system tasks so RR and Periodic tasks have a chance to run after
	Task_Terminate();
}

// This function is passed as an argument to tasks made when testing publishing
void run_publish()
{
	Service_Publish(service, 5);
	Task_Terminate();
}

// Periodic tasks need a special publish function to pass to a task as they have to be able to pass control to a different task
void run_publish_periodic()
{
	for(;;)
	{
		Service_Publish(service, 5);
		Task_Next();
	}
}

// This test creates NUMTASKS round robin tasks which all subscribe to a service, then a RR task is made to publish to those tasks
void test_services()
{
	service = Service_Init();
	for (int i = 0; i < NUMTASKS; i++)
	{
		Task_Create_RR(run_subscribe, 0);
	}
	Task_Create_RR(run_publish, 5);
}

void test_error_LED()
{
	// Arduino LED light should flash 7 times
	OS_Abort();
}

// This function is used to prove that the _delay_25ms is not accurate
void test_now()
{
	for (;;)
	{
		uint16_t time_before = Now();
		_delay_25ms();
		uint16_t time_after = Now();
		USART_Send_int(time_after - time_before);
		USART_Send_string("\n");
	}
}

// This test just makes NUMTASKS round robin tasks which each are just executing an infinite loop
void test_RR()
{
	for (int i = 0; i < NUMTASKS; i++)
	{
		Task_Create_RR(inf_loop, i);
	}
}

// This test creates a round robin task, which delays then makes a system task
// This proves that tasks can make other tasks
void test_SYSTEM()
{
	Task_Create_RR(create_SYSTEM_task, 0);
}

// This test is generating a valid schedule of periodic tasks
void test_generate_good_schedule()
{
	Task_Create_Periodic(finished, 0, 100, 90, 0);
	Task_Create_Periodic(finished, 1, 50, 40, 150);
	//Task_Create_Periodic(inf_loop, 2, 25, 20, 170);
}

// This function is used for periodic tasks so they just pass up control as soon as they get it.
void finished()
{
	for(;;)
	{
		Task_Next();
	}
}

// This function is used to test periodic termination. It will go through its period 5 times, then terminate
void run_perodic_5_then_terminate()
{
	for(int i = 0; i < 5; i++)
	{
		Task_Next();
	}
	USART_Send_string("Terminating\n");
	Task_Terminate();
}

// Same as above but 10 times
void run_perodic_10_then_terminate()
{
	for(int i = 0; i < 10; i++)
	{
		Task_Next();
	}
	USART_Send_string("Terminating\n");
	Task_Terminate();
}

// This function creates an invalid schedule of periodic tasks. Will throw an error.
void test_generate_bad_schedule()
{
	Task_Create_Periodic(finished, 0, 100, 90, 0);
	Task_Create_Periodic(finished, 1, 100, 90, 50);
}

// This function will make RR and system tasks subscribe, then RR/Periodic/System task publish
// Functionality changes based on which lines are commented out and which behaviour is desired
void test_publish_all()
{
	service = Service_Init();
	Task_Create_RR(run_subscribe, 2);
	Task_Create_RR(run_subscribe, 3);
	Task_Create_RR(run_subscribe, 4);
	// Task_Create_System(run_subscribe, 0);
	// Task_Create_System(run_subscribe, 1);
	Task_Create_RR(run_publish, 6);
	// Task_Create_Periodic(finished, 5, 50, 40, 0);
	// Task_Create_Periodic(run_publish_periodic, 6, 100, 90, 100);
	// Task_Create_System(run_publish, 1);
}

// This function sets up the interrupt on TIMER3 so it runs every 5ms. Called when we want to test publishing from an interrupt
void test_publish_from_interrupt()
{	
	TCCR3B |= (_BV(CS11));
	TIMSK3 |= _BV(OCIE3A);
	OCR3A = TCNT3 + TICK_CYCLES;
	/* Clear flag. */
	TIFR3 = _BV(OCF3A);
	
	// Enable LED: PIn 13
	DDRC = (uint8_t)(_BV(DDC0));
	PORTC = 0;
}

// The interrupt which is setup by the above function. The interrupt is run every 5ms but we wanted to publish every second to
// give tasks time to run and show up better on the logic analyzer, so publish is called every 200 ticks
void TIMER3_COMPA_vect(void)
{
	if(start_interrupt < 200)
	{
		start_interrupt++;
	}
	else
	{
		PORTC = DDRC;
		PORTC = 0;
		start_interrupt = 0;
		Service_Publish(service, 5);
	}
	//USART_Send_string("Interrupt\n");
	OCR3A += TICK_CYCLES;
}

void inf_loop()
{
	for (;;)
	{
	}
}

// Called by a round robin task to prove that you can make a task from within a task
void create_SYSTEM_task()
{
	Task_Create_System(delay_and_terminate, 0);
}

// Delays for 1s then terminates the task
void delay_and_terminate()
{
	for(int i = 0; i < 40; i++)
	{
		_delay_ms(25);
	}
	Task_Terminate();
}

// This function is called when we were profiling our code. The different sections are commented out depending on what we were profiling
void profile()
{
	// kernel_update_ticker
	// test_generate_good_schedule();
	
	/*
	// enqueue_periodic
	Task_Create_Periodic(finished, 0, 100, 90, 0);
	Task_Create_Periodic(finished, 1, 50, 40, 150);
	Task_Create_Periodic(finished, 2, 25, 20, 200);
	Task_Create_Periodic(finished, 3, 25, 20, 110);
	Task_Create_Periodic(finished, 3, 5, 5, 145);
	*/
	
	/*
	// dequeue_periodic
	Task_Create_Periodic(run_perodic_10_then_terminate, 0, 100, 90, 0);
	Task_Create_Periodic(run_perodic_10_then_terminate, 1, 50, 40, 150);
	Task_Create_Periodic(run_perodic_5_then_terminate, 2, 25, 20, 200);
	Task_Create_Periodic(run_perodic_5_then_terminate, 3, 25, 20, 110);
	Task_Create_Periodic(run_perodic_5_then_terminate, 3, 5, 5, 145);
	*/
	
	// now
	// OS_Abort();
	
	// Task_Create_System
	// Task_Create_System(inf_loop, 0);
	
	// Task_Create_RR
	// Task_Create_RR(inf_loop, 0);
	
	// Task_Create_Periodic
	// Task_Create_Periodic(finished, 0, 100, 90, 0);
	
	// Service_Init
	// service = Service_Init();
	
	// Service_Subscribe, Service_Publish
	test_publish_all();
}
