/*
 * RoombaVer3.h
 *
 * Created: 2015-04-04 5:33:45 PM
 *  Author: Daniel
 */ 

#include "radio/radio.h"

#ifndef ROOMBAVER3_H_
#define ROOMBAVER3_H_

#ifdef __cplusplus
extern "C" {
	#endif

	void roomba_Drive( int16_t velocity, int16_t radius );
	// void radio_rxhandler(uint8_t pipenumber);
	void ir_rxhandler();
	void handleRoombaPacket(radiopacket_t *packet);
	void handleIRPacket(radiopacket_t *packet);
	void handleStatusPacket(radiopacket_t *packet);
	void rr_roomba_controler();
	void per_roomba_timeout();
	int r_main();
	void roombaShoot();
	void roombaShootRR();
	void spinAndBlink();
	void handleRadioPackets();

	#ifdef __cplusplus
}
#endif

#endif /* ROOMBAVER3_H_ */
